package com.task.newapp.activity;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import com.task.newapp.R;
import com.task.newapp.adapter.NewslistAdapter;
import com.task.newapp.databinding.ActivityMainBinding;
import com.task.newapp.model.Article;
import com.task.newapp.recycleview.RecyclerTouchListener;
import com.task.newapp.viewrepository.MainViewModel;

import java.util.ArrayList;
import java.util.List;


public class MainActivity extends AppCompatActivity {

    RecyclerView rvNewItems;
    private RecyclerView.LayoutManager layoutManager;
    NewslistAdapter newslistAdapter;
    private Context context;
    private ActivityMainBinding activityMainBinding;
    private MainViewModel mainViewModel;
    private RecyclerTouchListener onTouchListener;
    List<Article> articleslists=new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityMainBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);

        context = MainActivity.this;

        rvNewItems = findViewById(R.id.rv_new_item_list);

        mainViewModel = ViewModelProviders.of(this).get(MainViewModel.class);

        getNewListItems();

        // bind RecyclerView
        rvNewItems.setLayoutManager(new LinearLayoutManager(this));
        rvNewItems.setHasFixedSize(true);

        mainViewModel = ViewModelProviders.of(this).get(MainViewModel.class);
        newslistAdapter = new NewslistAdapter();
        rvNewItems.setAdapter(newslistAdapter);

        onTouchListener = new RecyclerTouchListener(this, rvNewItems);
        onTouchListener.setClickable(new RecyclerTouchListener.OnRowClickListener() {
            @Override
            public void onRowClicked(int position) {
                String url = articleslists.get(position).getUrl();
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
            }

            @Override
            public void onIndependentViewClicked(int independentViewID, int position) {
            }
        })
                .setLongClickable(true, new RecyclerTouchListener.OnRowLongClickListener() {
                    @Override
                    public void onRowLongClicked(int position) {
                    }
                });

        onTouchListener.setClickable(true);
        rvNewItems.setAdapter(newslistAdapter);
        rvNewItems.addOnItemTouchListener(onTouchListener);

    }

    private void getNewListItems() {
        mainViewModel.getStock(context).observe(this, new Observer<List<Article>>() {
            @Override
            public void onChanged(@Nullable List<Article> articleslist) {
                newslistAdapter.setNewsItems((ArrayList<Article>) articleslist,context);
                articleslists=articleslist;
            }
        });
    }

}
