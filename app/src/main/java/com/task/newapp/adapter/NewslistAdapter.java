package com.task.newapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;
import com.bumptech.glide.Glide;
import com.task.newapp.R;
import com.task.newapp.databinding.TotalStockItemsBinding;
import com.task.newapp.model.Article;
import java.util.ArrayList;
import java.util.List;

public class NewslistAdapter extends RecyclerView.Adapter<NewslistAdapter.Viewholde> {
    List<Article> articleslist = new ArrayList<>();
    Context context;

    @NonNull
    @Override
    public NewslistAdapter.Viewholde onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        TotalStockItemsBinding totalStockItemsBinding =
                DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()),
                        R.layout.total_stock_items, viewGroup, false);
        return new Viewholde(totalStockItemsBinding);

    }

    @Override
    public void onBindViewHolder(@NonNull NewslistAdapter.Viewholde holder, int position) {

        Article articlelist = articleslist.get(position);
        holder.totalStockItemsBinding.setArticleList(articlelist);

        Glide.with(context)
                .load(articlelist.getUrlToImage())
                .into(holder.totalStockItemsBinding.ivNewsIcob);

    }

    @Override
    public int getItemCount() {
        if (articleslist != null) {
            return articleslist.size();
        } else {
            return 0;
        }

    }

    public void setNewsItems(ArrayList<Article> articleslist,Context context) {
        this.articleslist = articleslist;
        this.context=context;
        notifyDataSetChanged();

    }

    public class Viewholde extends RecyclerView.ViewHolder {
        private TotalStockItemsBinding totalStockItemsBinding;


        public Viewholde(@NonNull TotalStockItemsBinding totalStockItemsBinding) {
            super(totalStockItemsBinding.getRoot());
            this.totalStockItemsBinding = totalStockItemsBinding;

        }
    }
}
