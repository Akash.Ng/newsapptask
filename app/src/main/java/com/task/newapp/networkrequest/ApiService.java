package com.task.newapp.networkrequest;

import com.task.newapp.model.NewsModel;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ApiService {

    @GET("top-headlines")
    Observable<NewsModel> getLatestNews(@Query("sources") String source, @Query("apiKey") String apiKey);
}
