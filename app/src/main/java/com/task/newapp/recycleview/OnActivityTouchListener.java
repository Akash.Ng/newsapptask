package com.task.newapp.recycleview;

import android.view.MotionEvent;

public interface OnActivityTouchListener {
    void getTouchCoordinates(MotionEvent ev);
}
