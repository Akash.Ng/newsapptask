package com.task.newapp.viewrepository;

import android.app.Application;
import android.content.Context;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.task.newapp.model.Article;

import java.util.List;

public class MainViewModel extends AndroidViewModel {
    private NewRepository newRepository;
    private Context context;

    public MainViewModel(@NonNull Application application) {
        super(application);
        newRepository = new NewRepository();
    }

    public LiveData<List<Article>> getStock(Context context) {
        this.context=context;
        return newRepository.getMutableLiveData(context);
    }

}

