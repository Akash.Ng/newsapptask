package com.task.newapp.viewrepository;

import android.widget.Toast;
import android.content.Context;
import androidx.lifecycle.MutableLiveData;
import com.task.newapp.helper.InternetCheck;
import com.task.newapp.model.Article;
import com.task.newapp.model.NewsModel;
import com.task.newapp.networkrequest.ApiClient;
import com.task.newapp.networkrequest.ApiService;
import java.util.ArrayList;
import java.util.List;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class NewRepository {
    private ArrayList<Article> articleslist = new ArrayList<>();
    private MutableLiveData<List<Article>> mutableLiveData = new MutableLiveData<>();
    private MutableLiveData<NewsModel> mustock = new MutableLiveData<>();
    final ApiService apiService = ApiClient.getClient();
    Context context;

    public NewRepository() {
    }

    public MutableLiveData<List<Article>> getMutableLiveData(Context context) {
        this.context = context;
        new InternetCheck(new InternetCheck.Consumer() {
            @Override
            public void accept(Boolean internet) {
                if (internet) {
                    apiService.getLatestNews("techcrunch","5185685620484b6dbccb66694cc88f9e")
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(new Observer<NewsModel>() {
                                @Override
                                public void onSubscribe(Disposable d) {

                                }

                                @Override
                                public void onNext(NewsModel newsModel) {
                                    if (newsModel != null && newsModel.getArticleslist() != null) {
                                         articleslist= (ArrayList<Article>) newsModel.getArticleslist();
                                        mutableLiveData.setValue(articleslist);
                                    }
                                }

                                @Override
                                public void onError(Throwable e) {
                                    Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();
                                }

                                public void onComplete() {

                                }
                            });
                } else {
                    Toast.makeText(context, "Check Internet Connection", Toast.LENGTH_SHORT).show();
                }
            }
        });
        return mutableLiveData;
    }
}
