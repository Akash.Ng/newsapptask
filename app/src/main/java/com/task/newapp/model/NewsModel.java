package com.task.newapp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class NewsModel {
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("totalResults")
    @Expose
    private Integer totalResults;
    @SerializedName("articles")
    @Expose
    private List<Article> articleslist;

    public NewsModel() {

    }

    public NewsModel(String status, Integer totalResults, List<Article> articleslist) {
        this.status = status;
        this.totalResults = totalResults;
        this.articleslist = articleslist;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getTotalResults() {
        return totalResults;
    }

    public void setTotalResults(Integer totalResults) {
        this.totalResults = totalResults;
    }

    public List<Article> getArticleslist() {
        return articleslist;
    }

    public void setArticleslist(List<Article> articleslist) {
        this.articleslist = articleslist;
    }

    @Override
    public String toString() {
        return "NewsModel{" +
                "status='" + status + '\'' +
                ", totalResults=" + totalResults +
                ", articleslist=" + articleslist +
                '}';
    }
}
